#Emdyp API for news - Documentation files

#1 PREPARATIONS: KNOW YOUR FRANCHISE SUBDOMAIN

First, you have to know your franchise subdomain, is the letters that
goes before "newsbtc.com", for example, for latin america franchise
we have http://lta.newsbtc.com/ where our franchise subdomain is "lta".

The franchises subdomain currently supported are (consult [Emdyp API Reference](api_reference.md) for more info):

* ph for (philiphines)
* lta for (latin-america)
* ca for (canada)
* www or global for (global)
* uk for (united kingdom)
* us for (united states)
* af for (africa)

Emdyp API for news is a json api that shows a collection of data about 
the last news of newsbtc franchises to people who wants to consult
newsbtc feed on their webpages.

#2 MODIFY HTML CODE

Check the document "basicnewsreader.html" and find the lines before:

    <!-- define franchise to show news -->
    <span id="franchisename">LTA</span>

The code is currently being setup for latin-america franchise (lta subdomain),
then you have to change the LTA for your franchise's subdomain. for example if
we want to set-up the widget for africa franchise (af subdomain), we replace
previous lines for:

    <!-- define franchise to show news -->
    <span id="franchisename">AF</span>

The replaced text could be written in normal letters or capital letters, check
this example working [here](basicnewsreader_af_example.html). 

## IMPORTANT INFORMATION RELATED

about global newsbtc, you can write "www" or just "GLOBAL" as you prefer.

#3 WATCH FRANCHISE BAR RUNNING

To watch your franchise bar running, download the document "basicnewsreader.html",
open it with a basic text editor like notepad for windows, any html editor like
geany or bluefish or some linux editor like mousepad or gedit, change the franchise's
subdomain explained beore in numeral 2, save and then open the file with your web browser

#4 IF WORKS, WHY DONT SHARE NOW?

We're currently working on pack the bar in a better way so it can be embeed in a webpage,
looking for that the posterior mantainance and improvement of the widget will not involve the user, 
i mean, we dont have to say sysadmins: hey dude, this is the new code to embeed, sorry the one you
have now is not currently working, we want once the sysadmin of we website embeed our news bar
do not have to worry anymore about changing anything.

also, we want to do a better graphic design, we want to listen your opinions.

#5 WHAT IF DOESNT WORK?

Emdyp.me developed this software, we are a start-up that works with technology and 
language services, visit us at http://emdyp.me, you can write us at [mailbox@emdyp.me](mailto:mailbox@emdyp.me)
or to [latinamerica@newsbtc.com](mailto:latinamerica@newsbtc.com), or follow us at @emdyp.

every comment, help or donation is welcome, you can support us by helping with the development,
designing cool id templates, giving us your feedback.

###Donations
* bitcoin: 1B3RmdZJbR4ArU8MdKL2UPCuacAnRMMmNe

we support bogota coinfest 2015, even in blockchain :) http://www.cryptograffiti.info/?txnr=2238