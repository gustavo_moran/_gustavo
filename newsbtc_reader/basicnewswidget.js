function getnews(mode) {
  /* variable to consult api */
  var xmlHttp;
  /* root of api url to consult */
  var apiDir = "http://api-emdyp.rhcloud.com/news/last/"
  /* get the element to put the title and url of the consulted news */
  var newspublisher = document.getElementById('emdypnewsreader');
  var franchisepublisher = document.getElementById('franchisename');
  /* recover the franchise subdomain */
  var franchise = franchisepublisher.innerHTML.toLowerCase();

  /* check api if we are runing for the first time the code or if we finished
     showing the ten first latest articles of the franchise */
  if (mode === 'last' || newsbtcCounter%10 === 0) {
	  if (window.XMLHttpRequest){xmlHttp=new XMLHttpRequest();}
      var urlDir = apiDir + franchise;
      xmlHttp.open("GET", urlDir, true);
      xmlHttp.onload = function(e) {
		  franchise_news = JSON.parse(xmlHttp.response);
                  /* show the last article's title of the franchise */
		  newspublisher.innerHTML = franchise_news[0]['name']
                  /* show the last article's url inside the a tag */
		  newspublisher.href = franchise_news[0]['url']
      };
  xmlHttp.send();
  /* oherwise show the other articles */
  } else {
          /* show the article's title */
	  newspublisher.innerHTML = franchise_news[newsbtcCounter%10]['name']
          /* show the article's url in the a tag */
	  newspublisher.href = franchise_news[newsbtcCounter%10]['url']
	  }
          /* increase the counter that tell us which article we should show and if
             we need to call the api again for fresh values */
	  newsbtcCounter++;
};